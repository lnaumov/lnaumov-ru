export default {
  env: {},
  head: {
    htmlAttrs: {
      lang: 'en'
    },
    title: 'Leonid Naumov - personal website/portfolio',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Personal website/portfolio of frontend developer'
      },
      { name: 'msapplication-TileColor', content: '#da532c' },
      { name: 'theme-color', content: '#ffffff' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/apple-touch-icon.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicon-32x32.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon-16x16.png'
      },
      {
        rel: 'mask-icon',
        href: '/safari-pinned-tab.svg',
        color: '#5bbad5'
      }
    ]
  },
  loading: { color: '#3B8070' },
  css: ['~/assets/css/main.css', { src: '~static/global.scss', lang: 'scss' }],
  build: {
    extend(config, { isDev, isClient }) {
      const vueRule = config.module.rules.find(rule => rule.test.test('.vue'));
      vueRule.use = [
        {
          loader: vueRule.loader,
          options: vueRule.options
        },
        {
          loader: 'vue-svg-inline-loader',
          options: {
            inline: {
              keyword: 'data-svg-inline',
              strict: true
            },
            sprite: {
              keyword: 'svg-sprite',
              strict: true
            },
            dataAttributes: [],
            removeAttributes: ['alt', 'src'],
            md5: true,
            xhtml: false,
            svgo: {
              plugins: [
                {
                  cleanupIDs: false,
                  removeViewBox: false
                }
              ]
            }
          }
        }
      ];
      delete vueRule.loader;
      delete vueRule.options;
    }
  },
  modules: ['@nuxtjs/axios'],
  axios: {},
  plugins: [],
  router: {
    base: '/'
  },
  terser: {
    sourceMap: true
  }
};
