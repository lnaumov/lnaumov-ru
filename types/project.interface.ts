export interface Project {
  name: string;
  description: string;
  url: string;
  img: string;
  githubUrl?: string;
  technologiesNames?: string[];
  tags?: string[];
}
