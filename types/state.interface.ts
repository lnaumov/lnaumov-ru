import { Technology } from './index';
import { ChemicalElement, Project } from '~/types';

export interface RootState {
  skills: string[];
  projects: Project[];
  technologies: Map<string, Technology>;
  chemicalElements: { [symbol: string]: ChemicalElement };
  chemicalSymbols: Array<any>;
}
