export interface ChemicalElement {
  name: string;
  atomic_mass: number;
  number: number;
  symbol: string;
  shells: Array<number>;
}
