export interface Technology {
  name: string;
  img: string;
  version?: string;
  url?: string;
}
