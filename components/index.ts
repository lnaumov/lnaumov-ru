import SkillComponent from './SkillComponent.vue';
import FlaskComponent from './FlaskComponent.vue';
import ChemicalTitleComponent from './ChemicalTitleComponent.vue';
import ChemicalSymbol from './ChemicalSymbol.vue';
import ProjectComponent from './ProjectComponent.vue';

export {
  SkillComponent,
  FlaskComponent,
  ChemicalTitleComponent,
  ChemicalSymbol,
  ProjectComponent,
};
