import { MutationTree, ActionTree } from 'vuex';
import PeriodicElements from './static/PeriodicElements.json';
import Projects from './static/Projects.json';
import Skills from './static/Skills.json';
import Technologies from './static/Technologies.json';
import { Technology } from '~/types/index';
import { RootState, ChemicalElement, Project } from '~/types';

export const state = (): RootState => {
  const state = {
    skills: [],
    projects: [],
    technologies: new Map(),
    chemicalElements: {},
    chemicalSymbols: []
  };
  return state;
};

export const mutations: MutationTree<RootState> = {
  setSkills(state: RootState, skills: string[]): void {
    state.skills = skills;
  },

  setProjects(state: RootState, projects: Project[]): void {
    state.projects = projects;
  },

  setTechnologies(
    state: RootState,
    technologies: Map<string, Technology>
  ): void {
    state.technologies = technologies;
  },

  setChemicalElements(
    state: RootState,
    elements: { [symbol: string]: ChemicalElement }
  ): void {
    state.chemicalElements = elements;
  },

  setChemicalSymbols(state: RootState, elements: Array<string>): void {
    state.chemicalSymbols = elements;
  }
};

export const actions: ActionTree<RootState, RootState> = {
  async nuxtServerInit({ commit }, context) {
    const skills: string[] = <string[]>Skills;
    const projects: Project[] = <Project[]>Projects;
    const technologies: Map<string, Technology> = new Map(<
      Map<string, Technology>
    >(<any>Technologies));
    const elements: { [symbol: string]: ChemicalElement } = PeriodicElements;
    const chemicalSymbols: Array<string> = Object.keys(elements);

    commit('setChemicalElements', elements); // full chemical objects
    commit('setChemicalSymbols', chemicalSymbols); //only chemical symbols, for example, 'O', 'Na', 'Fe'
    commit('setSkills', skills);
    commit('setProjects', projects);
    commit('setTechnologies', technologies);
  }
};
