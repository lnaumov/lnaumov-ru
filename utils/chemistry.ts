import { ChemicalElement } from '~/types';

/******* Chemistry helpers *******/

// Split text by words and assign chemical style to them
export const addChemistryElementsToWords = (
  text: string,
  wrapTag: string = 'span',
  chemicalSymbols: string[],
  chemicalElements: { [symbol: string]: ChemicalElement },
  type: 'symbol' | 'title'
): string => {
  const words = text.split(' ');
  let resultHtml: string;

  if (words.length === 1) {
    resultHtml = addChemistryElementToWord(
      words[0],
      chemicalSymbols,
      chemicalElements,
      type
    );
  } else {
    const newWords = words.map(word =>
      addChemistryElementToWord(word, chemicalSymbols, chemicalElements, type)
    );
    resultHtml = newWords
      .map(word => `<${wrapTag}>${word}</${wrapTag}>`)
      .join(' ');
  }

  return resultHtml;
};

// Find chemical pattern in word and style it
export const addChemistryElementToWord = (
  word: string,
  chemicalSymbols: string[],
  chemicalElements: { [symbol: string]: ChemicalElement },
  type: 'symbol' | 'title'
): string => {
  // Loop over array of chemical symbols, like 'Na', 'O', 'Fe', etc.
  for (let index = 0; index < chemicalSymbols.length; index++) {
    const symbol = chemicalSymbols[index];
    const { atomic_mass, number, shells } = chemicalElements[symbol];
    // console.log({ atomic_mass, number, shells });

    // console.log(symbol);

    if (word.toLowerCase().includes(symbol.toLowerCase())) {
      let newHtml: string;

      switch (type) {
        // Symbol makes chemical part of the word greenish
        case 'symbol':
          newHtml = `<span class="symbol">${symbol}</span>`;
          break;
        // Title puts big box-styled block with info about element around symbol in the word
        case 'title':
          newHtml = `
            <span class="element">${symbol}<span class="atomic-mass">
              ${atomic_mass.toPrecision(5)}</span>
              <span class="number">${number}</span><span class="shells">
              ${shells.join('-')}</span>
            </span>
            `;
          break;
        default:
          newHtml = `<span class="symbol">${symbol}</span>`;
      }

      // replace our simple text with chemistry symbol
      const pattern = new RegExp(symbol, 'i');
      return word.replace(pattern, newHtml);
    }
  }
  return word;
};
