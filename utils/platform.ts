// Check platform
export const isSafari = (): boolean => {
  if (process.browser) {
    return !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
  }
  return false;
};

export const is_iOS = (): boolean => {
  if (process.browser) {
    const iDevices = [
      'iPad Simulator',
      'iPhone Simulator',
      'iPod Simulator',
      'iPad',
      'iPhone',
      'iPod',
    ];

    if (!!navigator.platform) {
      while (iDevices.length) {
        if (navigator.platform === iDevices.pop()) {
          return true;
        }
      }
    }
  }
  return false;
};
